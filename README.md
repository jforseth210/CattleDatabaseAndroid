# CattleDB for Android
This is an obsolete project. It's a client app for CattleDB written for Android using Android's native developer tools. It's no longer recieving updates, and parts of it are no longer compatible with the CattleDB api. See [CattleDB React Native](https://gitlab.com/jforseth210/cattledb-react-native) instead.
