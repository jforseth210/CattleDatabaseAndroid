package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import tech.jforseth.cattledatabase.databinding.FragmentLoginAccountInfoBinding;

import static android.content.Context.MODE_PRIVATE;

/*
    This fragment is responsible for prompting for the user's
    username and password, checking if it works, and storing it
    in shared preferences before allowing the user to proceed to
    the MainActivity
 */
public class LoginAccountInfoFragment extends Fragment {

    private FragmentLoginAccountInfoBinding binding;
    private SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentLoginAccountInfoBinding.inflate(inflater, container, false);

        preferences = getActivity().getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences preferences = getActivity().getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);
        binding.editTextUsername.setText(preferences.getString("username", ""));
        binding.editTextPassword.setText(preferences.getString("password", ""));
        binding.buttonPrevious.setOnClickListener(previous_view -> {
            writePreferences();
            goToPreviousFragment();
        });
        binding.buttonSubmit.setOnClickListener(submit_view -> {
            writePreferences();
            testCredentials();
        });
    }

    private void testCredentials() {
        new MakeHTTPRequest(
                "test_credentials",
                response -> {
                    SharedPreferences preferences = getActivity().getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);
                    SharedPreferences.Editor pref_editor = preferences.edit();
                    pref_editor.putBoolean("logged_in", true);
                    pref_editor.apply();
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    getActivity().startActivity(i);
                    getActivity().finish();
                },
                requireActivity()
        ).execute();
    }

    private void writePreferences() {
        SharedPreferences.Editor pref_editor = preferences.edit();
        pref_editor.putString("username", binding.editTextUsername.getText().toString());
        pref_editor.putString("password", binding.editTextPassword.getText().toString());
        pref_editor.apply();
    }

    private void goToPreviousFragment() {
        NavHostFragment.findNavController(LoginAccountInfoFragment.this)
                .navigate(R.id.action_Second2Fragment_to_First2Fragment);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}