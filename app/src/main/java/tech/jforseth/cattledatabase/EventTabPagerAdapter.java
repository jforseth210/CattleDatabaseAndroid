package tech.jforseth.cattledatabase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.json.JSONObject;
/*
    I'm going to be honest, I'm not really sure what these PagerAdapters do.
    Basically, this just creates new InformationSublistFragments for each tab
    in the EventInformationActivity.
 */
public class EventTabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    JSONObject jsonInfo;
    Context context;

    public EventTabPagerAdapter(FragmentManager fm, int numberOfTabs, JSONObject jsonInfo, Context context) {
        super(fm);
        this.tabCount = numberOfTabs;
        this.context = context;
        this.jsonInfo = jsonInfo;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                InformationSublistFragment cowsFragment = new InformationSublistFragment().newInstance(jsonInfo, "cows", 0, 0, 1);
                cowsFragment.setListener(this::loadCow);
                return cowsFragment;
            case 1:
                InformationSublistFragment transactionsFragment = new InformationSublistFragment().newInstance(jsonInfo, "transactions", 0, 1, 2);
                transactionsFragment.setListener(this::loadTransaction);
                return transactionsFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return tabCount;
    }

    private void loadCow(View cowView){
        final String calf_id = ((TextView) cowView).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) cowView).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "cows/cow",
                calf_id,
                calf_response -> {
                    Intent new_intent = new Intent(context, CowInformationActivity.class);
                    new_intent.putExtra("tag_number", ((TextView) cowView).getText().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", calf_response.toString());
                    context.startActivity(new_intent);
                    ((Activity)context).finish();

                },
                context
        ).execute();
    }
    private void loadTransaction(View transactionView){
        final String transaction_id = ((TextView) transactionView).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) transactionView).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "transactions/transaction",
                transaction_id,
                transaction_response -> {
                    Intent new_intent = new Intent(context, TransactionInformationActivity.class);
                    new_intent.putExtra("transaction_id", ((TextView) transactionView).getText().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", transaction_response.toString());
                    context.startActivity(new_intent);
                    ((Activity)context).finish();

                },
                context
        ).execute();
    }
}
