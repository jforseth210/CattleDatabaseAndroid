package tech.jforseth.cattledatabase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
/*
    This fragment displays a list of sub-items shown within an information view.
    For example, a cow would have three informationSublistFragments:
        - Offspring
        - Events
        - Transactions
 */
public class InformationSublistFragment extends Fragment {
    private static final String ARG_JSON_RESPONSE = "json_response";
    private static final String ID_INDEX = "id_index";
    private static final String PRIMARY_INDEX = "primary_index";
    private static final String SECONDARY_INDEX = "secondary_index";
    private static final String ARRAY_KEY = "array_key";

    private ArrayList<CardView> cards;
    private View.OnClickListener listener;
    public InformationSublistFragment() {
        // Required empty public constructor
    }

    /*
        Create a new InformationSublistFragment
        jsonResponse - The JSON for the parent
        key - The key to look up this sublist in the parent json object
        id_index - Given an individual sub thing as an array, what is the index of the identifier?
        primary_index - Given an individual sub thing as an array, what is the index of the primary text to display?
        secondary_index - Given an individual sub thing as an array, what is the index of the secondary text to display?

        Example:
        jsonResponse =
        {
            "key" : {
                ["primary text", "id", "secondary text"],
            }
            ... other (irrelevant) information
        }
        key = "key"
        id_index = 1
        primary_index = 0
        secondary_index = 2
     */
    public static InformationSublistFragment newInstance(JSONObject jsonResponse, String key, int id_index, int primary_index, int secondary_index) {
        InformationSublistFragment fragment = new InformationSublistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_JSON_RESPONSE, jsonResponse.toString());
        args.putInt(ID_INDEX, id_index);
        args.putInt(PRIMARY_INDEX, primary_index);
        args.putInt(SECONDARY_INDEX, secondary_index);
        args.putString(ARRAY_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_sublist, container, false);

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONObject(getArguments().getString(ARG_JSON_RESPONSE)).getJSONArray(getArguments().getString(ARRAY_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] idArray = new String[jsonArray.length()];
        String[] primaryTextArray = new String[jsonArray.length()];
        String[] secondaryTextArray = new String[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                idArray[i] = jsonArray.getJSONArray(i).getString(getArguments().getInt(ID_INDEX));
                primaryTextArray[i] = jsonArray.getJSONArray(i).getString(getArguments().getInt(PRIMARY_INDEX));
                secondaryTextArray[i] = jsonArray.getJSONArray(i).getString(getArguments().getInt(SECONDARY_INDEX));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ListViewGenerator listGenerator = new ListViewGenerator(getActivity(), idArray, primaryTextArray, secondaryTextArray, listener);

        cards = listGenerator.makeList();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayout list = getView().findViewById(R.id.cowInformationOffspringViewLinearLayout);
        for (CardView card : this.cards){
            list.addView(card);
        }
    }
    /*
        I was unable to pass a listener as an argument to the fragment, so this setter
        is used instead. When the primary TextView is clicked, this listener will be called.
     */
    public void setListener(View.OnClickListener listener){
        this.listener = listener;
    }
}