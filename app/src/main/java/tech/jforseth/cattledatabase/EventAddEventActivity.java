package tech.jforseth.cattledatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*
    This activity displays a form when the user wants to create a new
    event. It prompts for all of the information necessary to create a
    event and then sends that information to the server.
 */
public class EventAddEventActivity extends AppCompatActivity {
    private EditText nameInput, descriptionInput;
    private Button submitButton;
    private DatePicker date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_add_event);

        submitButton = findViewById(R.id.addEventSubmitButton);
        date = findViewById(R.id.addEventDateInput);
        nameInput = ((EditText)findViewById(R.id.addEventNameInput));
        descriptionInput = ((EditText)findViewById(R.id.addEventDescriptionInput));
        submitButton.setOnClickListener(this::submit);
    }
    private void submit(View view){
        String name = nameInput.getText().toString().trim();
        String description = descriptionInput.getText().toString().trim();
        String iso_date = makeIsoDate(date);
        Map newEventDict = new HashMap();
        newEventDict.put("name", name);
        newEventDict.put("date", iso_date);
        newEventDict.put("description", description);

        JSONObject newEventJSON = new JSONObject(newEventDict);
        new MakeHTTPRequest(
                "events/add",
                newEventJSON.toString(),
                success_response -> {
                    Toast.makeText(this, "Event Added", Toast.LENGTH_SHORT).show();
                    this.finish();
                },
                this
        ).execute();
    }
    private String makeIsoDate(DatePicker date){
        String year = String.valueOf(date.getYear());
        String month = String.valueOf(date.getMonth() + 1);
        String day = String.valueOf(date.getDayOfMonth());

        if (month.length() < 2)
            month = "0" + month;
        if (day.length() < 2)
            day = "0" + day;

        return year + "-" + month + "-" + day;
    }
}