package tech.jforseth.cattledatabase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.json.JSONObject;

// I hate Java. So, so much...
public class TransactionTabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    JSONObject jsonInfo;
    Context context;

    public TransactionTabPagerAdapter(FragmentManager fm, int numberOfTabs, JSONObject jsonInfo, Context context) {
        super(fm);
        this.tabCount = numberOfTabs;
        this.context = context;
        this.jsonInfo = jsonInfo;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                InformationSublistFragment eventsFragment = new InformationSublistFragment().newInstance(jsonInfo, "event", 0, 1, 2);
                eventsFragment.setListener(this::loadEvent);
                return eventsFragment;
            case 1:
                InformationSublistFragment cowsFragment = new InformationSublistFragment().newInstance(jsonInfo, "cows", 0, 0, 1);
                cowsFragment.setListener(this::loadCow);
                return cowsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    private void loadEvent(View eventView) {
        final String event_id = ((TextView) eventView).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) eventView).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "events/event",
                event_id,
                event_response -> {
                    Intent new_intent = new Intent(context, EventInformationActivity.class);
                    new_intent.putExtra("event_id", ((TextView) eventView).getHint().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", event_response.toString());
                    context.startActivity(new_intent);
                    ((Activity) context).finish();

                },
                context
        ).execute();

    }

    private void loadCow(View cowView) {
        final String cow_id = ((TextView) cowView).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) cowView).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "cows/cow",
                cow_id,
                cow_responses -> {
                    Intent new_intent = new Intent(context, CowInformationActivity.class);
                    new_intent.putExtra("tag_number", ((TextView) cowView).getText().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", cow_responses.toString());
                    context.startActivity(new_intent);
                    ((Activity) context).finish();

                },
                context
        ).execute();
    }
}
