package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionInformationActivity extends AppCompatActivity {
    private FloatingActionButton mEditFab, mAddRemoveCowsFab, mChangeAmountFab, mDeleteTransactionFab, mChangeNameFab, mChangeDescriptionFab, mChangeToFromFab;
    private TextView mAddRemoveCowsActionText, mChangeAmountActionText, mDeleteTransactionActionText, mChangeNameActionText, mChangeDescriptionActionText, mChangeToFromActionText;
    private Boolean isAllFabsVisible;

    private TextView transactionNameTextView, dateTextView, amountTextView, toFromTextView, descriptionTextView;

    private Intent intent;
    private JSONObject response;
    private String transactionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_information);

        mEditFab = findViewById(R.id.transaction_edit_fab);

        mAddRemoveCowsFab = findViewById(R.id.add_remove_cows_fab);
        mChangeAmountFab = findViewById(R.id.change_amount_fab);
        mDeleteTransactionFab = findViewById(R.id.delete_transaction_fab);
        mChangeNameFab = findViewById(R.id.change_name_fab);
        mChangeDescriptionFab = findViewById(R.id.change_description_fab);
        mChangeToFromFab = findViewById(R.id.change_to_from_fab);

        mAddRemoveCowsActionText = findViewById(R.id.add_remove_cows_action_text);
        mChangeAmountActionText = findViewById(R.id.change_amount_action_text);
        mDeleteTransactionActionText = findViewById(R.id.delete_transaction_action_text);
        mChangeNameActionText = findViewById(R.id.change_name_action_text);
        mChangeDescriptionActionText = findViewById(R.id.change_description_action_text);
        mChangeToFromActionText = findViewById(R.id.change_to_from_action_text);

        intent = getIntent();

        transactionNameTextView = findViewById(R.id.transactionInformationTransactionNameTextView);

        dateTextView = findViewById(R.id.transactionInformationDateTextView);
        amountTextView = findViewById(R.id.transactionInformationAmountTextView);
        descriptionTextView = findViewById(R.id.transactionInformationDescriptionTextView);
        toFromTextView = findViewById(R.id.transactionInformationToFromTextView);
        try {
            response = new JSONObject(intent.getExtras().getString("json_data", ""));
            transactionId = response.getString("transaction_id");

            transactionNameTextView.setText(response.getString("name"));
            dateTextView.setText(response.getString("date"));
            amountTextView.setText(response.getString("price"));
            descriptionTextView.setText(response.getString("description"));
            toFromTextView.setText(response.getString("tofrom"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        hideFabs();
        isAllFabsVisible = false;

        mEditFab.setOnClickListener(view -> {
            toggleFabs();
        });

        mDeleteTransactionFab.setOnClickListener(this::showDeleteTransactionDialog);
        mAddRemoveCowsFab.setOnClickListener(this::startAddRemoveCows);
        mChangeNameFab.setOnClickListener(this::showChangeNameDialog);
        mChangeDescriptionFab.setOnClickListener(this::showChangeDescriptionDialog);
        mChangeToFromFab.setOnClickListener(this::showChangeToFromDialog);
        mChangeAmountFab.setOnClickListener(this::showChangeAmountDialog);

        final ViewPager viewPager =
                findViewById(R.id.cowInfoViewPager);

        TabLayout transactionInformationTabs = findViewById(R.id.transactionInformationTabs);
        final PagerAdapter adapter = new TransactionTabPagerAdapter
                (getSupportFragmentManager(),
                        transactionInformationTabs.getTabCount(), response,this);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(transactionInformationTabs));
        transactionInformationTabs.setOnTabSelectedListener(new
                                                                    TabLayout.OnTabSelectedListener() {
                                                                        @Override
                                                                        public void onTabSelected(TabLayout.Tab tab) {
                                                                            viewPager.setCurrentItem(tab.getPosition());
                                                                        }

                                                                        @Override
                                                                        public void onTabUnselected(TabLayout.Tab tab) {

                                                                        }

                                                                        @Override
                                                                        public void onTabReselected(TabLayout.Tab tab) {

                                                                        }

                                                                    });


    }

    private void showDeleteTransactionDialog(View view) {
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Delete Transaction")
                    .setMessage("Are you sure you want to delete " + response.getString("name") + "? This action is IRREVERSIBLE")
                    .setNegativeButton("No", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Yes", (dialog1, which) -> {
                        deleteTransaction(transactionId);
                        AlertDialog success_dialog = null;
                        try {
                            success_dialog = new AlertDialog.Builder(this)
                                    .setTitle("Deleted")
                                    .setMessage(response.getString("name") + " has been deleted")
                                    .setPositiveButton("Ok", null)
                                    .create();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        success_dialog.show();
                        this.finish();
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void deleteTransaction(String transactionId) {
        new MakeHTTPRequest(
                "transactions/delete",
                transactionId,
                this
        ).execute();
    }

    private void startAddRemoveCows(View view) {
        new MakeHTTPRequest(
                "cows/get_list",
                this::getTransactionsCows,
                this
        ).execute();


    }

    private void getTransactionsCows(JSONObject getCowsResponse) {
        new MakeHTTPRequest(
                "transactions/transaction/get_cows",
                transactionId,
                transactionGetCowsResponse -> {
                    JSONArray getCowsJson = new JSONArray();
                    JSONArray transactionGetCowsJson = new JSONArray();
                    List<KeyPairBoolData> cows = null;
                    try {
                        getCowsJson = getCowsResponse.getJSONArray("cows");
                        transactionGetCowsJson = transactionGetCowsResponse.getJSONArray("cows");
                        if (getCowsJson != null && transactionGetCowsJson != null) {
                            cows = makeCowList(getCowsJson, transactionGetCowsJson);
                        }

                        MultiSpinnerSearch multiSelectSpinnerWithSearch = makeMultiSpinnerSearch(cows);
                        showAddRemoveCowsDialog(multiSelectSpinnerWithSearch);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this
        ).execute();
    }

    private void showAddRemoveCowsDialog(MultiSpinnerSearch multiSelectSpinnerWithSearch) throws JSONException {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Update Cows")
                .setView(multiSelectSpinnerWithSearch)
                .setMessage("Tap below to select the cows involved with " + response.getString("name"))
                .setNegativeButton("Cancel", (dialog1, which) -> {
                    //Do nothing
                })
                .setPositiveButton("Update", (dialog1, which) -> {
                    addRemoveCows(multiSelectSpinnerWithSearch);
                })
                .create();
        dialog.show();
    }

    private List<KeyPairBoolData> makeCowList(JSONArray getCowsJson, JSONArray transactionGetCowsJson) throws JSONException {
        List<KeyPairBoolData> cows = new ArrayList<>();
        for (int i = 0; i < getCowsJson.length(); i++) {
            KeyPairBoolData cow = new KeyPairBoolData();
            cow.setName(getCowsJson.getJSONArray(i).getString(0));
            cow.setId(i);
            Boolean selected = false;
            for (int j = 0; j < transactionGetCowsJson.length(); j++) {
                if (getCowsJson.getJSONArray(i).getString(0).equals(transactionGetCowsJson.getJSONArray(j).getString(0))) {
                    selected = true;
                }
            }
            cow.setSelected(selected);
            cows.add(cow);
        }
        return cows;
    }

    private void addRemoveCows(MultiSpinnerSearch multiSelectSpinnerWithSearch) {
        ArrayList<String> selectedCows = new ArrayList<>();
        List<KeyPairBoolData> kpbdItems = multiSelectSpinnerWithSearch.getSelectedItems();
        for (int i = 0; i < kpbdItems.size(); i++) {
            selectedCows.add(kpbdItems.get(i).getName());
        }

        Map updateCowsDict = new HashMap();
        updateCowsDict.put("transaction_id", transactionId);
        updateCowsDict.put("cows", selectedCows);
        JSONObject updateCowsJson = new JSONObject(updateCowsDict);
        new MakeHTTPRequest(
                "transactions/transaction/update_cows",
                updateCowsJson.toString(),
                this
        ).execute();
    }

    private MultiSpinnerSearch makeMultiSpinnerSearch(List<KeyPairBoolData> cows) {
        MultiSpinnerSearch multiSelectSpinnerWithSearch = new MultiSpinnerSearch(this);

        // Pass true If you want searchView above the list. Otherwise false. default = true.
        multiSelectSpinnerWithSearch.setSearchEnabled(true);

        // A text that will display in search hint.
        multiSelectSpinnerWithSearch.setSearchHint("Select Cows");

        // Set text that will display when search result not found...
        multiSelectSpinnerWithSearch.setEmptyTitle("No matching cows");

        // If you will set the limit, this button will not display automatically.
        multiSelectSpinnerWithSearch.setShowSelectAllButton(true);

        //A text that will display in clear text button
        multiSelectSpinnerWithSearch.setClearText("Close & Clear");

        multiSelectSpinnerWithSearch.setItems(cows, new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
            }
        });
        return multiSelectSpinnerWithSearch;
    }

    private void showChangeNameDialog(View view) {

        EditText input = new EditText(this);
        try {
            input.setText(response.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Transaction Name")
                    .setView(input)
                    .setMessage("Enter a new name for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String new_name = input.getText().toString().trim();
                        changeName(new_name);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void changeName(String new_name) {
        Map newNameDict = new HashMap();
        newNameDict.put("transaction_id", transactionId);
        newNameDict.put("new_name", new_name);
        JSONObject newNameJson = new JSONObject(newNameDict);

        new MakeHTTPRequest(
                "transactions/change_name",
                newNameJson.toString(),
                chang_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Name Changed")
                                .setMessage(response.getString("name") + " is now " + new_name)
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.transactionInformationTransactionNameTextView)).setText(new_name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }

    private void showChangeDescriptionDialog(View view) {
        EditText input = new EditText(this);
        try {
            input.setText(response.getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Transaction Description")
                    .setView(input)
                    .setMessage("Enter a new description for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String new_description = input.getText().toString().trim();
                        changeDescription(new_description);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void changeDescription(String new_description) {
        Map newDescriptionDict = new HashMap();
        newDescriptionDict.put("transaction_id", transactionId);
        newDescriptionDict.put("new_name", new_description);
        JSONObject newNameJson = new JSONObject(newDescriptionDict);

        new MakeHTTPRequest(
                "transactions/change_description",
                newNameJson.toString(),
                chang_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Description Changed")
                                .setMessage(response.getString("name") + "'s description has been updated.")
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.transactionInformationDescriptionTextView)).setText(new_description);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }
    private void showChangeToFromDialog(View view){
        EditText input = new EditText(this);
        try {
            input.setText(response.getString("tofrom"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Transaction To/From")
                    .setView(input)
                    .setMessage("Enter a new buyer/seller for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String new_to_from = input.getText().toString().trim();
                        changeToFrom(new_to_from);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }
    private void changeToFrom(String new_to_from){
        Map newToFromDict = new HashMap();
        newToFromDict.put("transaction_id", transactionId);
        newToFromDict.put("new_to_from", new_to_from);
        JSONObject newToFromJson = new JSONObject(newToFromDict);

        new MakeHTTPRequest(
                "transactions/change_to_from",
                newToFromJson.toString(),
                chang_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("To/From Changed")
                                .setMessage(response.getString("name") + " is now to/from " + new_to_from)
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.transactionInformationToFromTextView)).setText(new_to_from);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }
    private void showChangeAmountDialog(View view){
        EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        try {
            input.setText(response.getString("price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Amount")
                    .setView(input)
                    .setMessage("Enter a new amount " + response.getString("name") + ". Positive for income, negative for expenses.")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String new_amount = input.getText().toString().trim();
                        changeAmount(new_amount);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();

    }
    private void changeAmount(String new_amount){
        String cleaned_amount = new_amount.replace("$","").replace(",","");
        System.out.println(cleaned_amount);
        Map newAmountDict = new HashMap();
        newAmountDict.put("transaction_id", transactionId);
        newAmountDict.put("new_amount", cleaned_amount);
        JSONObject newAmountJson = new JSONObject(newAmountDict);

        new MakeHTTPRequest(
                "transactions/change_amount",
                newAmountJson.toString(),
                chang_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Amount Changed")
                                .setMessage(response.getString("name") + " is now " + new_amount)
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.transactionInformationAmountTextView)).setText(new_amount);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }
    private void toggleFabs() {
        if (!isAllFabsVisible) {
            showFabs();
            isAllFabsVisible = true;
        } else {
            hideFabs();
            isAllFabsVisible = false;
        }
    }

    private void showFabs() {
        mAddRemoveCowsFab.show();
        mChangeAmountFab.show();
        mDeleteTransactionFab.show();
        mChangeNameFab.show();
        mChangeDescriptionFab.show();
        mChangeToFromFab.show();
        mAddRemoveCowsActionText.setVisibility(View.VISIBLE);
        mChangeAmountActionText.setVisibility(View.VISIBLE);
        mDeleteTransactionActionText.setVisibility(View.VISIBLE);
        mChangeNameActionText.setVisibility(View.VISIBLE);
        mChangeDescriptionActionText.setVisibility(View.VISIBLE);
        mChangeToFromActionText.setVisibility(View.VISIBLE);
    }

    private void hideFabs() {
        mAddRemoveCowsFab.hide();
        mChangeAmountFab.hide();
        mDeleteTransactionFab.hide();
        mChangeNameFab.hide();
        mChangeDescriptionFab.hide();
        mChangeToFromFab.hide();
        mAddRemoveCowsActionText.setVisibility(View.GONE);
        mChangeAmountActionText.setVisibility(View.GONE);
        mDeleteTransactionActionText.setVisibility(View.GONE);
        mChangeDescriptionActionText.setVisibility(View.GONE);
        mChangeNameActionText.setVisibility(View.GONE);
        mChangeToFromActionText.setVisibility(View.GONE);
    }
}