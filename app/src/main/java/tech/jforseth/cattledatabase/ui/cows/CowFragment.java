package tech.jforseth.cattledatabase.ui.cows;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tech.jforseth.cattledatabase.CowInformationActivity;
import tech.jforseth.cattledatabase.ListViewGenerator;
import tech.jforseth.cattledatabase.MakeHTTPRequest;
import tech.jforseth.cattledatabase.CowAddCowActivity;
import tech.jforseth.cattledatabase.databinding.FragmentCowsBinding;
/*
    The list of cows show in the Main Activity. This fragment loads the list of cows,
    provides the option to reload them, and switches to CowAddCowActivity and
    CowInformationActivity when necessary.
 */
public class CowFragment extends Fragment {
    private FragmentCowsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentCowsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        loadCows();

        ImageView refreshButton = binding.refreshButton;
        refreshButton.setOnClickListener(
                view -> {
                    Toast.makeText(getActivity(), "Reloading!", Toast.LENGTH_SHORT).show();
                    loadCows();
                });

        FloatingActionButton mAddCowFab = binding.addCowFab;
        mAddCowFab.setOnClickListener(
                view -> {
                    Intent i = new Intent(getActivity(), CowAddCowActivity.class);
                    this.startActivity(i);
                });

        return root;
    }

    private void loadCows() {
        new MakeHTTPRequest(
                "cows/get_list",
                this::displayCows,
                requireActivity()
        ).execute();
    }

    private void displayCows(JSONObject response) {
        JSONArray cows;
        try {
            // Get JSON array
            cows = response.getJSONArray("cows");
            // Make it into cards
            ArrayList<CardView> cards = makeCards(cows);
            // Empty the list
            binding.cowList.removeAllViews();
            //Populate it with cards
            for (CardView card : cards) {
                binding.cowList.addView(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadCow(View view) {
        final String tagNumber = ((TextView) view).getHint().toString();
        Toast.makeText(requireActivity(), "Loading: " + tagNumber, Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "cows/cow",
                tagNumber,
                response -> {
                    Intent new_intent = new Intent(requireActivity(), CowInformationActivity.class);
                    new_intent.putExtra("tag_number", tagNumber);
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", response.toString());
                    requireActivity().startActivity(new_intent);
                },
                requireActivity()
        ).execute();
    }

    public ArrayList<CardView> makeCards(JSONArray cows) throws JSONException {
        String[] idArray = new String[cows.length()];
        String[] primaryTextArray = new String[cows.length()];
        String[] secondaryTextArray = new String[cows.length()];

        for (int i = 0; i < cows.length(); i++) {
            idArray[i] = cows.getJSONArray(i).getString(0);
            primaryTextArray[i] = cows.getJSONArray(i).getString(0);
            secondaryTextArray[i] = cows.getJSONArray(i).getString(1);
        }

        ListViewGenerator generator = new ListViewGenerator(
                requireActivity(),
                idArray,
                primaryTextArray,
                secondaryTextArray,
                this::loadCow
        );
        return generator.makeList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

