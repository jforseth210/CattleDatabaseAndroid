package tech.jforseth.cattledatabase.ui.events;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tech.jforseth.cattledatabase.ListViewGenerator;
import tech.jforseth.cattledatabase.MakeHTTPRequest;
import tech.jforseth.cattledatabase.databinding.FragmentEventsBinding;
import tech.jforseth.cattledatabase.EventAddEventActivity;
import tech.jforseth.cattledatabase.EventInformationActivity;

/*
    The list of events show in the Main Activity. This fragment loads the list of events,
    provides the option to reload them, and switches to EventAddEventActivity and
    EventInformationActivity when necessary.
 */
public class EventFragment extends Fragment {
    String currentEvent;
    private FragmentEventsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentEventsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        loadEvents();
        ImageView refreshButton = binding.refreshButton;
        refreshButton.setOnClickListener(view -> {
            Toast.makeText(getActivity(), "Reloading!", Toast.LENGTH_SHORT).show();
            loadEvents();
        });

        FloatingActionButton mAddEventFab = binding.addeventFab;
        mAddEventFab.setOnClickListener(
                view -> {
                    Intent i = new Intent(getActivity(), EventAddEventActivity.class);
                    this.startActivity(i);
                });

        return root;
    }

    private void loadEvents() {
        new MakeHTTPRequest(
                "events/get_list",
                this::displayEvents,
                requireActivity()
        ).execute();
    }

    private void displayEvents(JSONObject response) {
        JSONArray events;
        try {
            events = response.getJSONArray("events");
            ArrayList<CardView> cards = makeList(events);
            binding.eventList.removeAllViews();
            for (CardView card : cards) {
                binding.eventList.addView(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<CardView> makeList(JSONArray events) throws JSONException {
        String[] idArray = new String[events.length()];
        String[] primaryTextArray = new String[events.length()];
        String[] secondaryTextArray = new String[events.length()];
        for (int i = 0; i < events.length(); i++) {
            idArray[i] = events.getJSONArray(i).getString(0);
            primaryTextArray[i] = events.getJSONArray(i).getString(1);
            secondaryTextArray[i] = events.getJSONArray(i).getString(2);
        }
        ListViewGenerator generator = new ListViewGenerator(requireActivity(), idArray, primaryTextArray, secondaryTextArray, this::loadEvent);
        return generator.makeList();
    }

    private void loadEvent(View view) {
        currentEvent = ((TextView) view).getHint().toString();
        String endpoint = "events/event";
        String data = currentEvent;
        new MakeHTTPRequest(
                endpoint,
                data,
                response -> {
                    Intent i = new Intent(getActivity(), EventInformationActivity.class);
                    i.putExtra("event_id", currentEvent);
                    i.putExtra("json_data", response.toString());
                    getActivity().startActivity(i);
                },
                getActivity()
        ).execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

