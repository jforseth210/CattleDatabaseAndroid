package tech.jforseth.cattledatabase.ui.transactions;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tech.jforseth.cattledatabase.ListViewGenerator;
import tech.jforseth.cattledatabase.MakeHTTPRequest;
import tech.jforseth.cattledatabase.databinding.FragmentTransactionsBinding;
import tech.jforseth.cattledatabase.TransactionAddTransactionActivity;
import tech.jforseth.cattledatabase.TransactionInformationActivity;

/*
    The list of transactions shown in the Main Activity. This fragment loads the list of events,
    provides the option to reload them, and switches to TransactionAddTransactionActivity and
    TransactionInformationActivity when necessary.
 */
public class TransactionFragment extends Fragment {

    RequestQueue requestQueue;
    String currentTransaction;
    private FragmentTransactionsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentTransactionsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        requestQueue = Volley.newRequestQueue(requireActivity());
        loadTransactions();
        ImageView refreshButton = binding.refreshButton;
        refreshButton.setOnClickListener(view -> {
            Toast.makeText(getActivity(), "Reloading!", Toast.LENGTH_SHORT).show();
            loadTransactions();
        });

        FloatingActionButton mAddTransactionFab = binding.addtransactionFab;
        mAddTransactionFab.setOnClickListener(
                view -> {
                    Intent i = new Intent(getActivity(), TransactionAddTransactionActivity.class);
                    this.startActivity(i);
                });

        return root;
    }

     private void loadTransactions() {
        new MakeHTTPRequest(
                "transactions/get_list",
                this::displayTransactions,
                requireActivity()
        ).execute();
    }

     private void displayTransactions(JSONObject response) {
        JSONArray transactions;
        try {
            transactions = response.getJSONArray("transactions");
            ArrayList<CardView> cards = makeCards(transactions);
            binding.transactionList.removeAllViews();
            for (CardView card : cards) {
                binding.transactionList.addView(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private ArrayList<CardView> makeCards(JSONArray transactions) throws JSONException{
        String[] idArray = new String[transactions.length()];
        String[] primaryTextArray = new String[transactions.length()];
        String[] secondaryTextArray = new String[transactions.length()];
        for (int i = 0; i < transactions.length(); i++) {
            idArray[i] = transactions.getJSONArray(i).getString(0);
            primaryTextArray[i] = transactions.getJSONArray(i).getString(1);
            secondaryTextArray[i] = transactions.getJSONArray(i).getString(2);
        }
        ListViewGenerator generator = new ListViewGenerator(requireActivity(), idArray, primaryTextArray, secondaryTextArray, view -> {
            currentTransaction = ((TextView) view).getHint().toString();
            loadTransaction();
        });
        return generator.makeList();
    }
     private void loadTransaction() {
        String endpoint = "transactions/transaction";
        String data = currentTransaction;
        new MakeHTTPRequest(
                endpoint,
                data,
                response -> {
                    Intent i = new Intent(getActivity(), TransactionInformationActivity.class);
                    i.putExtra("transaction_id", currentTransaction);
                    i.putExtra("json_data", response.toString());
                    getActivity().startActivity(i);
                },
                getActivity()
        ).execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

