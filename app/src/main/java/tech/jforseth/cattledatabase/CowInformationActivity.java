package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
    Displays an individual cow, and provides toggleable FABs
    for a variety of possible actions for that cow.
    The tabbed lists are handled by CowTabPagerAdapter,
    and there are a number of other activities that deal with
    more complex operations.
 */
public class CowInformationActivity extends AppCompatActivity {
    // Informational TextViews
    private TextView ownerTextView, sireTextView, damTextView, sexTextView, tagNumberTextView;

    // FAB stuff
    private FloatingActionButton mEditFab, mAddParentFab, mAddCalfFab, mDeleteCowFab, mTransferOwnershipFab, mChangeTagNumberFab, mChangeSexFab;
    private TextView mAddParentActionText, mAddCalfActionText, mDeleteCowActionText, mTransferOwnershipActionText, mChangeTagNumberActionText, mChangeSexActionText;
    private Boolean isAllFabsVisible;

    private JSONObject response;
    private String tagNumber;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow_information);

        //Fab toggle logic


        // This FAB button is the Parent
        mEditFab = findViewById(R.id.cow_edit_fab);
        // FAB button
        mAddParentFab = findViewById(R.id.cow_add_parent_fab);
        mAddCalfFab = findViewById(R.id.cow_add_calf_fab);
        mDeleteCowFab = findViewById(R.id.cow_delete_cow_fab);
        mTransferOwnershipFab = findViewById(R.id.cow_transfer_ownership_fab);
        mChangeTagNumberFab = findViewById(R.id.cow_change_tag_number_fab);
        mChangeSexFab = findViewById(R.id.cow_change_sex_fab);

        // Also register the action name text, of all the FABs.
        mAddParentActionText = findViewById(R.id.cow_add_parent_action_text);
        mAddCalfActionText = findViewById(R.id.cow_add_calf_action_text);
        mDeleteCowActionText = findViewById(R.id.cow_delete_cow_action_text);
        mTransferOwnershipActionText = findViewById(R.id.cow_transfer_ownership_action_text);
        mChangeTagNumberActionText = findViewById(R.id.cow_change_tag_number_action_text);
        mChangeSexActionText = findViewById(R.id.cow_change_sex_action_text);

        hideFabs();
        isAllFabsVisible = false;

        // TextView registering
        ownerTextView = findViewById(R.id.cowInformationOwnerTextView);
        sexTextView = findViewById(R.id.cowInformationSexTextView);
        damTextView = findViewById(R.id.cowInformationDamTextView);
        sireTextView = findViewById(R.id.cowInformationSireTextView);
        tagNumberTextView = findViewById(R.id.cowInformationCowNameTextView);

        intent = getIntent();
        tagNumber = intent.getExtras().getString("tag_number", "");

        // Fab event listeners
        mEditFab.setOnClickListener(view -> {
            toggleFabs();
        });

        mAddParentFab.setOnClickListener(this::switchToAddParentActivity);
        mTransferOwnershipFab.setOnClickListener(this::switchToTransferOwnershipActivity);
        mDeleteCowFab.setOnClickListener(this::displayDeleteCowDialog);
        mChangeTagNumberFab.setOnClickListener(this::displayChangeTagDialog);
        mChangeSexFab.setOnClickListener(this::displayChangeSexDialog);
        mAddCalfFab.setOnClickListener(this::switchToAddCalfActivity);

        // Populating the activity

        populateActivity();

        // Java sourcery that has something to do with the tabbed sublists
        final ViewPager viewPager =
                findViewById(R.id.cowInfoViewPager);

        TabLayout cowInformationTabs = findViewById(R.id.transactionInformationTabs);
        final PagerAdapter pagerAdapter = new CowTabPagerAdapter
                (getSupportFragmentManager(),
                        cowInformationTabs.getTabCount(), response, this);
        viewPager.setAdapter(pagerAdapter);

        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(cowInformationTabs));
        cowInformationTabs.setOnTabSelectedListener(new
                                                            TabLayout.OnTabSelectedListener() {
                                                                @Override
                                                                public void onTabSelected(TabLayout.Tab tab) {
                                                                    viewPager.setCurrentItem(tab.getPosition());
                                                                }

                                                                @Override
                                                                public void onTabUnselected(TabLayout.Tab tab) {

                                                                }

                                                                @Override
                                                                public void onTabReselected(TabLayout.Tab tab) {

                                                                }

                                                            });
    }

    private void populateActivity() {
        try {
            response = new JSONObject(intent.getExtras().getString("json_data", ""));
            ownerTextView.setText(response.getString("owner"));
            sexTextView.setText(response.getString("sex"));
            damTextView.setText(response.getString("dam"));
            sireTextView.setText(response.getString("sire"));
            tagNumberTextView.setText(tagNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createParentLink(damTextView);
        createParentLink(sireTextView);
    }

    private void createParentLink(TextView view) {
        if (!view.getText().equals("N/A")) {
            view.setPaintFlags(view.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            view.setOnClickListener(this::loadParent);
        }
    }

    private void loadParent(View view) {
        final String parentTagNumber = ((TextView) view).getText().toString();
        Toast.makeText(this, "Loading: " + parentTagNumber, Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "cows/cow",
                parentTagNumber,
                sire_response -> {
                    Intent new_intent = new Intent(this, CowInformationActivity.class);
                    new_intent.putExtra("tag_number", parentTagNumber);
                    new_intent.putExtra("json_data", sire_response.toString());
                    this.startActivity(new_intent);
                    this.finish();

                },
                this
        ).execute();
    }

    private void switchToAddParentActivity(View view) {
        Intent i = new Intent(this, CowAddParentActivity.class);
        i.putExtra("tag_number", tagNumber);
        this.startActivity(i);
    }

    private void switchToAddCalfActivity(View view) {
        Intent i = new Intent(this, CowAddCalfActivity.class);
        i.putExtra("tag_number", tagNumber);
        this.startActivity(i);
    }

    private void switchToTransferOwnershipActivity(View view) {
        Intent i = new Intent(this, CowTransferOwnershipActivity.class);
        i.putExtra("tag_number", tagNumber);
        this.startActivity(i);
    }

    private void populateSexSpinner(Spinner sexSpinner) {
        new MakeHTTPRequest(
                "cows/sex_list",
                sex_list_response -> {
                    try {
                        JSONArray sexes_json = sex_list_response.getJSONArray("sexes");
                        String[] sexes = new String[sexes_json.length()];
                        for (int i = 0; i < sexes_json.length(); i++) {
                            sexes[i] = sexes_json.getString(i);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sexes);
                        sexSpinner.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this
        ).execute();
    }

    private void displayChangeSexDialog(View view) {
        final Spinner sexSpinner = new Spinner(this);
        populateSexSpinner(sexSpinner);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Change Sex")
                .setView(sexSpinner)
                .setNegativeButton("Cancel", (dialog1, which) -> {
                    //Do nothing
                })
                .setPositiveButton("Change", (dialog1, which) -> {
                    String sex = sexSpinner.getSelectedItem().toString();
                    changeSex(sex);
                })
                .create();
        dialog.show();
    }

    private void changeSex(String sex) {
        String tag_number = tagNumber;
        Map newTagDict = new HashMap();
        newTagDict.put("tag_number", tag_number);
        newTagDict.put("sex", sex);
        JSONObject newTagJSON = new JSONObject(newTagDict);

        new MakeHTTPRequest(
                "cows/change_sex",
                newTagJSON.toString(),
                change_tag_response -> {
                    AlertDialog success_dialog = new AlertDialog.Builder(this)
                            .setTitle("Sex Changed")
                            .setMessage(tagNumber + " is now a " + sex)
                            .setPositiveButton("Ok", null)
                            .create();
                    success_dialog.show();
                    sexTextView.setText(sex);
                },
                this
        ).execute();
    }

    private void displayChangeTagDialog(View view) {
        final EditText input = new EditText(this);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Change Tag Number")
                .setView(input)
                .setMessage("Enter a new tag number for " + tagNumber + ":")
                .setNegativeButton("Cancel", (dialog1, which) -> {
                    //Do nothing
                })
                .setPositiveButton("Change", (dialog1, which) -> {
                    changeTag(input);
                })
                .create();
        dialog.show();

    }

    private void changeTag(EditText input) {
        String new_tag = input.getText().toString().trim();
        String old_tag = tagNumber;
        Map newTagDict = new HashMap();
        newTagDict.put("old_tag", old_tag);
        newTagDict.put("new_tag", new_tag);
        JSONObject newTagJSON = new JSONObject(newTagDict);

        new MakeHTTPRequest(
                "cows/change_tag",
                newTagJSON.toString(),
                change_tag_response -> {
                    AlertDialog success_dialog = new AlertDialog.Builder(this)
                            .setTitle("Tag Number Changed")
                            .setMessage(tagNumber + " is now " + new_tag)
                            .setPositiveButton("Ok", null)
                            .create();
                    success_dialog.show();
                    tagNumberTextView.setText(new_tag);
                },
                this
        ).execute();
    }

    private void displayDeleteCowDialog(View view) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Delete Cow")
                .setMessage("Are you sure you want to delete " + tagNumber + "? This action is IRREVERSIBLE")
                .setNegativeButton("No", (dialog1, which) -> {
                    //Do nothing
                })
                .setPositiveButton("Yes", (dialog1, which) -> {
                    deleteCow(tagNumber);
                })
                .create();
        dialog.show();
    }

    private void deleteCow(String tagNumber) {
        new MakeHTTPRequest(
                "cows/delete",
                tagNumber,
                response -> {
                    AlertDialog success_dialog = new AlertDialog.Builder(this)
                            .setTitle("Deleted")
                            .setMessage(tagNumber + " has been deleted")
                            .setPositiveButton("Ok", null)
                            .create();
                    success_dialog.show();
                    this.finish();
                },
                this
        ).execute();
    }

    private void toggleFabs() {
        if (!isAllFabsVisible) {
            showFabs();
            isAllFabsVisible = true;
        } else {
            hideFabs();
            isAllFabsVisible = false;
        }

    }
    private void showFabs(){
        mAddParentFab.show();
        mAddCalfFab.show();
        mDeleteCowFab.show();
        mTransferOwnershipFab.show();
        mChangeTagNumberFab.show();
        mChangeSexFab.show();
        mAddParentActionText.setVisibility(View.VISIBLE);
        mAddCalfActionText.setVisibility(View.VISIBLE);
        mDeleteCowActionText.setVisibility(View.VISIBLE);
        mTransferOwnershipActionText.setVisibility(View.VISIBLE);
        mChangeTagNumberActionText.setVisibility(View.VISIBLE);
        mChangeSexActionText.setVisibility(View.VISIBLE);
    }
    private void hideFabs(){
        mAddParentFab.hide();
        mAddCalfFab.hide();
        mDeleteCowFab.hide();
        mTransferOwnershipFab.hide();
        mChangeTagNumberFab.hide();
        mChangeSexFab.hide();
        mAddParentActionText.setVisibility(View.GONE);
        mAddCalfActionText.setVisibility(View.GONE);
        mDeleteCowActionText.setVisibility(View.GONE);
        mChangeTagNumberActionText.setVisibility(View.GONE);
        mTransferOwnershipActionText.setVisibility(View.GONE);
        mChangeSexActionText.setVisibility(View.GONE);
    }
}