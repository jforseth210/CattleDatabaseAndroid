package tech.jforseth.cattledatabase;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import tech.jforseth.cattledatabase.databinding.FragmentLoginServerInfoBinding;

import static android.content.Context.MODE_PRIVATE;

/*
    This fragment is responsible for either scanning the network to
    determine the user's server information or, failing that,
    to prompt the user for that information.
 */
public class LoginServerInfoFragment extends Fragment {

    private FragmentLoginServerInfoBinding binding;
    private SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentLoginServerInfoBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences preferences = getActivity().getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        // If server preferences exist, populate the inputs with their values.
        binding.editTextLANAddress.setText(preferences.getString("server_LAN_address", ""));
        binding.editTextWANAddress.setText(preferences.getString("server_WAN_address", ""));

        binding.buttonFirst.setOnClickListener(previous_button -> {
            goToNextFragment();
        });
        binding.WANModeCheckBox.setOnClickListener(v -> {
            if (binding.WANModeCheckBox.isChecked()) {
                showWAN();
            } else {
                hideWAN();
            }
        });
        scan();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void scan() {
        NetworkSniffTask task = new NetworkSniffTask(LoginServerInfoFragment.this, getActivity());
        task.execute();
    }
    private void writePreferences(){
        SharedPreferences.Editor pref_editor = preferences.edit();
        pref_editor.putString("server_LAN_address", binding.editTextLANAddress.getText().toString());
        pref_editor.putString("server_WAN_address", binding.editTextWANAddress.getText().toString());
        pref_editor.apply();
    }
    private void hideWAN(){
        binding.textViewWANAddressHeader.setVisibility(View.GONE);
        binding.editTextWANAddress.setVisibility(View.GONE);
        binding.textViewWANAddressHelperText.setVisibility(View.GONE);
    }
    private void showWAN(){
        binding.textViewWANAddressHeader.setVisibility(View.VISIBLE);
        binding.editTextWANAddress.setVisibility(View.VISIBLE);
        binding.textViewWANAddressHelperText.setVisibility(View.VISIBLE);
    }
    public void goToNextFragment(){
        NavHostFragment.findNavController(LoginServerInfoFragment.this)
                .navigate(R.id.action_First2Fragment_to_Second2Fragment);
    }
}
