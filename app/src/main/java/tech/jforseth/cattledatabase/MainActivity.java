package tech.jforseth.cattledatabase;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tech.jforseth.cattledatabase.databinding.ActivityMainBinding;

/*
    The Main Activity. This holds CowFragment, EventFragment, and TransactionFragment
    as well as the navigation between them and the log out button.
 */
public class MainActivity extends AppCompatActivity {
    private final int[] CLIENT_VERSION = new int[]{0, 1, 0};

    private AppBarConfiguration mAppBarConfiguration;

    private SharedPreferences preferences;

    private TextView navUsername, navIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tech.jforseth.cattledatabase.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Don't know what this line is, or why Android Studio thinks it's an error, but the app crashes without it.
        setSupportActionBar(binding.appBarMain.toolbar);

        preferences = getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        if (!isLoggedIn()) {
            goToLoginScreen();
        }

        checkForUpdates();

        populateHeaderView();
        populateNavView(binding);

    }

    //Switch to the login activity
    private void goToLoginScreen() {
        Intent i = new Intent(this, LoginActivity.class);
        this.startActivity(i);
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    // The "three dots" menu. Currently only deals with logging out.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_log_out:
                logOut();

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /*
        If none of the account preferences are blank, and the logged_in
        boolean is set to true, then the user is logged in.
     */
    private Boolean isLoggedIn() {
        return
                !(preferences.getString("password", "").equals("") ||
                        preferences.getString("username", "").equals("") ||
                        preferences.getString("server_LAN_address", "").equals(""))
                        && preferences.getBoolean("logged_in", false);
    }

    /*
        When the user logs out, blank all of their credentials,
        and server information, set the logged_in boolean to false,
        and switch to the login activity.
    */
    private void logOut() {
        SharedPreferences.Editor pref_editor = preferences.edit();
        pref_editor.putString("username", "");
        pref_editor.putString("password", "");
        pref_editor.putString("server_LAN_address", "");
        pref_editor.putString("server_WAN_address", "");
        pref_editor.putBoolean("logged_in", false);
        pref_editor.apply();
        goToLoginScreen();
    }

    private void populateHeaderView() {
        NavigationView navigationViewEditable = findViewById(R.id.nav_view);
        View headerView = navigationViewEditable.getHeaderView(0);

        navUsername = headerView.findViewById(R.id.textViewLoginUsername);
        navUsername.setText(preferences.getString("username", ""));
        navIP = headerView.findViewById(R.id.textViewLoginIP);
        navIP.setText(preferences.getString("server_LAN_address", ""));
    }

    private void populateNavView(ActivityMainBinding binding) {
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_cows, R.id.nav_events, R.id.nav_transactions)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void checkForUpdates() {
        System.out.println("Checking for updates");
        MakeHTTPRequest update_request = new MakeHTTPRequest(
                "https://cattledb.jforseth.tech/versions.json",
                response -> {
                    System.out.println("Checked for updates");
                    System.out.println(response);
                    JSONObject versions = null;
                    try {
                        versions = response.getJSONObject("versions");
                        JSONArray clientVersions = versions.getJSONArray("client");
                        JSONArray latestClientVersion = clientVersions.getJSONArray(0);
                        if (
                                latestClientVersion.getInt(0) != CLIENT_VERSION[0] ||
                                        latestClientVersion.getInt(1) != CLIENT_VERSION[1] ||
                                        latestClientVersion.getInt(2) != CLIENT_VERSION[2]
                        ) {
                            AlertDialog dialog = new AlertDialog.Builder(this)
                                    .setTitle("Update Available")
                                    .setMessage("You're running CattleDB version " + CLIENT_VERSION[0] +
                                            "." + CLIENT_VERSION[1] + "." + CLIENT_VERSION[2] + ", but version " +
                                            latestClientVersion.getInt(0) + "." + latestClientVersion.getInt(1) +
                                            "." + latestClientVersion.getInt(2) + ", is available. Please visit " +
                                            "cattledb.jforseth.tech to download the latest version!"
                                    )
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    })
                                    .create();
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this
        );
        update_request.useAutomaticURL(false);
        update_request.execute();
    }
}
