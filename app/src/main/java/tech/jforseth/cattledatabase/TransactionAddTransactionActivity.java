package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
  Creates a new transaction. Gets a list of possible events to attach the transaction to.
  Prompts for other information, before passing to the information to the server.
 */
public class TransactionAddTransactionActivity extends AppCompatActivity {
    // Ordered list of possible events to attach the transaction to.
    private ArrayList<Integer> eventIds;
    // The event the transaction is currently set to be attached to.
    private String eventId;
    private Intent intent;
    private TextView eventLabel;
    private Spinner eventSpinner;
    private EditText amountInput, nameInput, toFromInput, descriptionInput;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_add_transaction);

        intent = getIntent();
        eventIds = new ArrayList<>();

        eventLabel = findViewById(R.id.addTransactionEventLabel);
        eventSpinner = findViewById(R.id.addTransactionEventSpinner);
        amountInput = findViewById(R.id.addTransactionAmount);
        nameInput = findViewById(R.id.addTransactionName);
        toFromInput = findViewById(R.id.addTransactionToFrom);
        descriptionInput = findViewById(R.id.addTransactionDescription);
        submitButton = findViewById(R.id.addTransactionSubmitButton);

        Bundle extras = intent.getExtras();
        if (extras != null) {
            // This is the event we will attach the transaction to.
            eventId = extras.getString("event_id");
            if (eventId != null) {
                // We know which event to attach the transaction to.
                eventLabel.setVisibility(View.GONE);
                eventSpinner.setVisibility(View.GONE);
            }
        } else {
            /*
             We don't know which event to attach the transaction to,
             ask the server for possible events.
             */
            loadEvents();
        }

        amountInput.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        submitButton.setOnClickListener(this::submit);
    }

    public String getEventId(int i) {
        return String.valueOf(eventIds.get(i));
    }

    private void loadEvents() {
        new MakeHTTPRequest(
                "events/get_list",
                response -> {
                    try {
                        String[] eventNames = parseEventsJson(response);
                        populateEventsSpinner(eventNames);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this
        ).execute();
    }

    private String[] parseEventsJson(JSONObject response) throws JSONException {
        JSONArray eventsJson = response.getJSONArray("events");
        String[] eventNames = new String[eventsJson.length()];

        for (int i = 0; i < eventsJson.length(); i++) {
            // The text that will be displayed to the user in the spinner
            eventNames[i] = eventsJson.getJSONArray(i).getString(2)
                    + ": " + eventsJson.getJSONArray(i).getString(1);


            /*
             This ArrayList is used to look up the event based on the index from
             the spinner. Example: [101, 300, 506, 149, 583]

             If the user selects the 0th item from the spinner, we know we're
             looking for event #101.
            */
            eventIds.add(eventsJson.getJSONArray(i).getInt(0));
        }
        return eventNames;
    }

    private void populateEventsSpinner(String[] eventNames) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, eventNames);
        eventSpinner.setAdapter(adapter);
        eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                eventId = getEventId(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void submit(View view) {
        String name = nameInput.getText().toString().trim();
        String amount = amountInput.getText().toString();
        String toFrom = toFromInput.getText().toString().trim();
        String description = descriptionInput.getText().toString().trim();

        Map newTransactionDict = new HashMap();
        newTransactionDict.put("event_id", eventId);
        newTransactionDict.put("name", name);
        newTransactionDict.put("amount", amount);
        newTransactionDict.put("to_from", toFrom);
        newTransactionDict.put("description", description);

        JSONObject newTransactionJson = new JSONObject(newTransactionDict);
        new MakeHTTPRequest(
                "transactions/add",
                newTransactionJson.toString(),
                transaction_added_response -> {
                    this.finish();
                },
                this
        ).execute();
    }
}
