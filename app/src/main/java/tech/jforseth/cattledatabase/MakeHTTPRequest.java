package tech.jforseth.cattledatabase;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
/*
  This class is responsible for all HTTP requests in CattleDB.
  It deals with authentication, LAN and WAN addresses and more.
  Uses the Volley library.
 */
public class MakeHTTPRequest {
  Response.Listener<org.json.JSONObject> listener;
  Response.ErrorListener errorListener;
  RequestQueue requestQueue;
  private final String endpoint;
  private final Context context;
  private String mode;
  private String url;
  private final String data;
  private int failures;
  private boolean shouldUseAutomaticUrl = true;

  //The main constructor. Sets everything up.
  public MakeHTTPRequest(String endpoint, String data,
                         Response.Listener<org.json.JSONObject> listener,
                         Response.ErrorListener errorListener,
                         Context context) {
    this.context = context;
    this.endpoint = endpoint;
    this.data = data;
    this.listener = listener;
    this.errorListener = errorListener;
    this.failures = 0;
    Cache cache = new DiskBasedCache(this.context.getCacheDir(), 1024 * 1024);
    Network network = new BasicNetwork(new HurlStack());
    requestQueue = new RequestQueue(cache, network);
    requestQueue.start();
  }

  //Alternative constructors. Listeners and data are optional.
  public MakeHTTPRequest(String endpoint, Response.Listener<org.json.JSONObject> listener,
                         Response.ErrorListener errorListener, Context context) {
    this(endpoint, "", listener, errorListener, context);
  }

  public MakeHTTPRequest(String endpoint, String data, Response.Listener<org.json.JSONObject> listener, Context context) {
    this(endpoint, data, listener, error -> {
    }, context);
  }

  public MakeHTTPRequest(String endpoint, Response.Listener<org.json.JSONObject> listener, Context context) {
    this(endpoint, "", listener, error -> {
    }, context);
  }

  public MakeHTTPRequest(String endpoint, String data, Context context) {
    this(endpoint, data, response -> {
    }, error -> {
    }, context);
  }

  public MakeHTTPRequest(String endpoint, Context context) {
    this(endpoint, "", response -> {
    }, error -> {
    }, context);
  }

  /*
    This function is responsible for creating the request url using stored server information, as well as the endpoint and
    WAN/LAN mode.
   */
  private void generateUrl() {
    SharedPreferences preferences = this.context.getSharedPreferences("tech.jforseth.CattleDatabase", MainActivity.MODE_PRIVATE);
      this.url = preferences.getString("server_" + this.mode + "_address", "")
              + "/api/"
              + this.endpoint;
  }
  /*
    This function creates the base64 encoding of the username and password used for authentication by
    flask_simplelogin.
   */
  @RequiresApi(api = Build.VERSION_CODES.O)
  public String generateAuthEncoding() {
    SharedPreferences preferences = this.context.getSharedPreferences("tech.jforseth.CattleDatabase", MainActivity.MODE_PRIVATE);
    String plain_auth = preferences.getString("username", "") + ":" + preferences.getString("password", "");
    String encoded_auth = Base64.getEncoder().encodeToString(plain_auth.getBytes());
    return encoded_auth;
  }
  /*
    Gets ready to send the request
   */
  public void execute() {
      // If the using a manually entered url, just send the request.
      if (!this.shouldUseAutomaticUrl) {
        sendRequest();
        return;
      }
      // Set it to LAN mode, generate a LAN url, request it.
      this.mode = "LAN";
      generateUrl();
      sendRequest();
      // Switch to WAN mode, generate, request
      this.mode = "WAN";
      generateUrl();
      sendRequest();
  }
  // Getter method for the current mode of the MakeHTTPRequest object,
  // either "WAN" or "LAN"
  public String getMode() {
    return this.mode;
  }

  // Setter method for the current mode of the MakeHTTPRequest object,
  // should be either "WAN" or "LAN"
  private void setMode(String mode) {
    this.mode = mode;
  }

  /*
    This is a bit hacky. Basically, this disables WAN/LAN mode switching
    and URL generation, and treats the endpoint provided in the constructor
    as a full URL.
   */
  public void useAutomaticURL(Boolean shouldUseAutomaticUrl) {
    this.url = this.endpoint;
    this.shouldUseAutomaticUrl = shouldUseAutomaticUrl;
  }

  // Getter method for the number of requests that've failed.
  private int getFailures() {
    return this.failures;
  }

  // Method to increment the number of failures
  private void incrementFailures() {
    this.failures++;
  }

  //  Getter method for the error listener
  public Response.ErrorListener getErrorListener() {
    return errorListener;
  }


  // The function that actually makes the request
  private void sendRequest() {
    JSONObject request_data = null;
    // Try to create a JSON object from the data
    try {
       request_data= new JSONObject(this.data);
    } catch (JSONException e) {
      /*
       If it's just a string, create a JSON Object like this for it:
       {
          "": "whatever the string is"
       }
       */
      try {
        request_data = new JSONObject();
        request_data.put("", this.data);
      } catch (JSONException jsonException) {
        // Something has gone horribly wrong.
        // Activate protocol xkcd2200
        jsonException.printStackTrace();
      }
    }
    JsonObjectRequest
            jsonObjectRequest
            = new JsonObjectRequest(
            Request.Method.POST,
            this.url,
            request_data,
            listener,
            error -> {
              // Only if both WAN and LAN requests fail should we do anything.
              if (getFailures() >= 1 || !this.shouldUseAutomaticUrl) {
                // Call the provided error listener, and print the stack trace.
                Response.ErrorListener errorListener = getErrorListener();
                errorListener.onErrorResponse(error);
                error.printStackTrace();
                try {
                  // If the error has a human-readable message display it in a toast.
                  Toast.makeText(this.context, "Error: " + error.getMessage().substring(error.getMessage().indexOf(":") + 2), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                  // If not, and it's an error we know, display a predefined error message toast.
                  if (error.getClass().getName().equals("com.android.volley.AuthFailureError"))
                    Toast.makeText(this.context, "Error: Authentication Failed", Toast.LENGTH_SHORT).show();
                  else if (error.getClass().getName().equals("com.android.volley.TimeoutError"))
                    Toast.makeText(this.context, "Error: Connection timed out", Toast.LENGTH_SHORT).show();
                  else if (error.getClass().getName().equals("com.android.volley.ServerError"))
                    Toast.makeText(this.context, "Server Error: Something is probably wrong with the host computer", Toast.LENGTH_SHORT).show();
                  else
                    // Otherwise, just call it an unknown error
                    Toast.makeText(this.context, "Error: Unknown error", Toast.LENGTH_SHORT).show();
                }
              } else {
                // If both requests haven't failed, don't worry yet. Increment the counter so we know if we need to worry later.
                incrementFailures();
              }
            }) {

      @RequiresApi(api = Build.VERSION_CODES.O)
      @Override
      public Map<String, String> getHeaders() {
        // Add the authentication header and let the server know we'll be sending JSON.
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Basic " + generateAuthEncoding());
        params.put("content-type", "application/json");
        return params;
      }
    };
    requestQueue.add(jsonObjectRequest);
  }
}
