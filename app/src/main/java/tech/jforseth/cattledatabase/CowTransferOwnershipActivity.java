package tech.jforseth.cattledatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
    Changes the owner of the cow, and also creates a new event with a transaction
    for the cow. Hence, the DatePicker and price EditText.
 */
public class CowTransferOwnershipActivity extends AppCompatActivity {
    private String tagNumber;
    private TextView tagNumberTextView;
    private EditText newOwner, priceInput, description;
    private DatePicker date;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        tagNumber = i.getExtras().getString("tag_number", "");

        setContentView(R.layout.activity_cow_transfer_ownership);
        newOwner = findViewById(R.id.transferOwnershipNewOwner);
        priceInput = findViewById(R.id.transferOwnershipPriceInput);
        date = findViewById(R.id.transferOwnershipDate);
        description = findViewById(R.id.transferOwnershipDetailsInput);

        submitButton = findViewById(R.id.transferOwnershipSubmitButton);
        submitButton.setOnClickListener(this::submit);

        tagNumberTextView = findViewById(R.id.transferOwnershipTagNumber);
        tagNumberTextView.setText("Transferring "+tagNumber);
    }
    private String makeIsoDate(DatePicker date){
        String year = String.valueOf(date.getYear());
        String month = String.valueOf(date.getMonth() + 1);
        String day = String.valueOf(date.getDayOfMonth());

        if (month.length() < 2)
            month = "0" + month;
        if (day.length() < 2)
            day = "0" + day;

        return year + "-" + month + "-" + day;
    }
    private void submit(View view){
        String iso_date = makeIsoDate(date);

        Map transferOwnershipDict = new HashMap<String, String>();
        transferOwnershipDict.put("tag_number", tagNumber);
        transferOwnershipDict.put("new_owner", newOwner.getText().toString().trim());
        transferOwnershipDict.put("price", priceInput.getText().toString().trim());
        transferOwnershipDict.put("date", iso_date);
        transferOwnershipDict.put("description", description.getText().toString().trim());

        JSONObject transferOwnershipJSON = new JSONObject(transferOwnershipDict);

        new MakeHTTPRequest(
                "cows/transfer_ownership",
                transferOwnershipJSON.toString(),
                response -> {
                    this.finish();
                },
                this
        ).execute();
    }
}