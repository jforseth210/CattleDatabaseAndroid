package tech.jforseth.cattledatabase;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import tech.jforseth.cattledatabase.databinding.ActivityLoginBinding;
/*
    This is the activity responsible for collecting the user's server
    and authentication information. However, most of the logic for
    doing so is in LoginAccountInfoFragment and LoginServerInfoFragment.
    All this does is set boolean saying that the user is not logged in
    when it starts. (If MainActivity sees that boolean is still false,
    the user is sent back here, since the app starts at MainActivity by
    default).
 */
public class LoginActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = this.getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        // Everything except these three lines is pre-generated boilerplate (AFAIK).
        SharedPreferences.Editor pref_editor = preferences.edit();
        pref_editor.putBoolean("logged_in", false);
        pref_editor.apply();


        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_login);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_login);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}