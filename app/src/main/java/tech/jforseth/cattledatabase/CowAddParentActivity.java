package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
    This activity provides two spinners for the user to select new parents for
    an existing cow. It does this by querying the server for all cows that could
    possibly be a dam or sire, and populating the respective spinners with those
    tag numbers.

    Once the user has made their selection CowAddParentActivity asks the server to
    update the cow's parents to the cows that've been selected.

    NO NEW COW OBJECTS ARE CREATED. ALLOWS USER TO SELECT AN EXISTING COW TO BE THE
    PARENT OF ANOTHER EXISTING COW.
 */
public class CowAddParentActivity extends AppCompatActivity {
    private Spinner sireSpinner, damSpinner;
    private Button submitButton;
    private TextView modifyingCowsParentsLabel;

    private String tagNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow_add_parent);

        Intent i = getIntent();
        tagNumber = i.getExtras().getString("tag_number", "");

        modifyingCowsParentsLabel = findViewById(R.id.modifyingCowsParentsLabel);
        damSpinner = findViewById(R.id.addParentDamSpinner);
        sireSpinner = findViewById(R.id.addParentSireSpinner);
        submitButton = findViewById(R.id.addParentSubmitButton);

        populateActivity();

        submitButton.setOnClickListener(this::submit);
    }
    private void populateActivity(){
        modifyingCowsParentsLabel.setText("Modifying " + tagNumber + "'s parents");
        getPossibleParents("dam");
        getPossibleParents("sire");
    }
    private void addParent(String newParentJSON) {
        new MakeHTTPRequest(
                "cows/add_parent",
                newParentJSON,
                this
        ).execute();
    }

    private void getPossibleParents(String type) {
        new MakeHTTPRequest(
                "cows/possible_parents",
                type,
                this::parsePossibleParents,
                this
        ).execute();
    }

    private void parsePossibleParents(JSONObject object) {
        try {
            String parentType = object.getString("parent_type");
            JSONArray parentsJson = object.getJSONArray("parents");
            String[] parents = new String[parentsJson.length() + 1];
            parents[0] = "N/A";
            for (int i = 0; i < parentsJson.length(); i++) {
                parents[i + 1] = parentsJson.getString(i);
            }
            populateParentSpinners(parentType, parents);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateParentSpinners(String parentType, String[] parents) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, parents);
            if (parentType.equals("dam")) {
                Spinner damSpinner = findViewById(R.id.addParentDamSpinner);
                damSpinner.setAdapter(adapter);
            } else {
                Spinner sireSpinner = findViewById(R.id.addParentSireSpinner);
                sireSpinner.setAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void submit(View view){
        String dam = damSpinner.getSelectedItem().toString();
        String sire = sireSpinner.getSelectedItem().toString();

        Map newParentDict = new HashMap();
        newParentDict.put("dam", dam);
        newParentDict.put("sire", sire);
        newParentDict.put("tag_number", tagNumber);

        JSONObject newParentJSON = new JSONObject(newParentDict);

        addParent(newParentJSON.toString());
        this.finish();
    }
}