package tech.jforseth.cattledatabase;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.text.format.Formatter;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static android.content.Context.MODE_PRIVATE;

/*
  This class is responsible for searching the network for CattleDB
  instances.
 */
public class NetworkSniffTask extends AsyncTask<Void, String, JSONObject> {


    private static boolean success = false;
    private Fragment fragment = null;
    private Context context = null;

    private ConnectivityManager connectivityManager;
    private NetworkInfo networkInfo;
    private WifiManager wifiManager;
    private WifiInfo wifiInfo;

    private SharedPreferences preferences;

    public NetworkSniffTask(Fragment fragment, Context context) {
        this.fragment = fragment;
        this.context = context;

        preferences = context.getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(JSONObject output) {
    }

    // Let the UI know which address we're scanning currently
    @Override
    protected void onProgressUpdate(String... values) {
        try {
            TextView textViewScanning = fragment.getView().findViewById(R.id.textViewScanning);
            textViewScanning.setText(values[0]);
        } catch (NullPointerException e) {

        }
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        setSuccess(false);

        Context context = this.context;

        if (context != null) {
            String prefix = getIPPrefix();
            for (int i = 0; i < 255; i++) {
                String testIp = prefix + i;

                if (hostIsUp(testIp)) {
                    checkIfIsCattleDBServer(testIp);
                }

                // If we found one, stop iteration
                if (getSuccess())
                    break;
            }
            // None of the hosts were a CattleDB server!
            publishProgress("Scan failed. Make sure CattleDB is running on the host computer and restart the app.");
        }

        return null;
    }
    private String getIPPrefix(){
        // This network scanning code is mostly copy-pasted from StackOverflow. I don't know how it works.
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String ipString = Formatter.formatIpAddress(ipAddress);

        return ipString.substring(0, ipString.lastIndexOf(".") + 1);
    }
    private boolean hostIsUp(String testIp) {
        try {
            InetAddress address = InetAddress.getByName(testIp);
            boolean reachable = address.isReachable(100);
            publishProgress("Scanning: " + testIp);
            return reachable;
        } catch(UnknownHostException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return false;
    }

    private void checkIfIsCattleDBServer(String testIp) {
        String url = "http://" + testIp + ":31523/api/get_server_info";
        MakeHTTPRequest request = new MakeHTTPRequest(url,
                response -> {
                    // We got server information. Quit searching, save the information, and switch to the next login fragment.
                    setSuccess(true);
                    writePreferences(response);
                    ((LoginServerInfoFragment) fragment).goToNextFragment();
                },
                context);
        /*
         We just want MakeHTTPRequest to treat this as an ordinary URL.
         Normally it loads server information from preferences, and we don't
         have that information yet!
         */
        request.useAutomaticURL(false);
        request.execute();
    }

    private void writePreferences(JSONObject response) {
        SharedPreferences.Editor pref_editor = preferences.edit();
        try {
            pref_editor.putString("server_LAN_address", "http://" + response.getString("LAN_address") + ":31523");
            pref_editor.putString("server_WAN_address", "http://" + response.getString("WAN_address") + ":31523");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pref_editor.apply();
    }

    private boolean getSuccess() {
        return success;
    }

    private void setSuccess(boolean s) {
        success = s;
    }
}