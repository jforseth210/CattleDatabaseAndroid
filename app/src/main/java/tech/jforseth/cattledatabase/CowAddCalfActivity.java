package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
    This activity is responsible for creating a new calf.
    Prompts for all of the required information to create
    a cow object (minus dam tag number).

    Provides a DatePicker and the option to automatically
    create "calved" and "born" events for the cow and calf
    respectively.
 */
public class CowAddCalfActivity extends AppCompatActivity {
    private Spinner sexSpinner, sireSpinner;
    private EditText ownerEditText, tagNumberEditText, addCalfOwnerEditText;
    private Switch calvedEvent, bornEvent;
    private DatePicker date;
    private Button submitButton;

    private String parentTagNumber;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow_add_calf);

        Intent intent = getIntent();
        parentTagNumber = intent.getExtras().getString("tag_number", "");

        sexSpinner = findViewById(R.id.addCalfSexSpinner);
        sireSpinner = findViewById(R.id.addCalfSireSpinner);
        bornEvent = findViewById(R.id.bornEventSwitch);
        calvedEvent = findViewById(R.id.calvedEventSwitch);
        ownerEditText = findViewById(R.id.addCalfOwnerEditText);

        tagNumberEditText = findViewById(R.id.addCalfTagNumberEditText);
        addCalfOwnerEditText = findViewById(R.id.addCalfOwnerEditText);
        date = findViewById(R.id.addCalfDatePicker);
        submitButton = findViewById(R.id.addCalfSubmitButton);

        preferences = getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        populateInputs();

        submitButton.setOnClickListener(this::submit);
    }

    private void populateInputs() {
        ownerEditText.setText(preferences.getString("username", ""));

        new MakeHTTPRequest(
                "cows/sex_list",
                this::populateSexSpinner,
                this
        ).execute();

        new MakeHTTPRequest(
                "cows/possible_parents",
                "sire",
                this::populateSireSpinner,
                this
        ).execute();
    }

    private void populateSexSpinner(JSONObject response) {
        try {
            JSONArray sexes_json = response.getJSONArray("sexes");
            String[] sexes = new String[sexes_json.length()];
            for (int i = 0; i < sexes_json.length(); i++) {
                sexes[i] = sexes_json.getString(i);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sexes);
            sexSpinner.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateSireSpinner(JSONObject response) {
        JSONArray parents_json = null;
        try {
            parents_json = response.getJSONArray("parents");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] parents = new String[parents_json.length() + 1];
        parents[0] = "Not Applicable";
        for (int i = 0; i < parents_json.length(); i++) {
            try {
                parents[i + 1] = parents_json.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayAdapter<String> sire_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, parents);

        sireSpinner.setAdapter(sire_adapter);
    }

    private void addCalf(JSONObject newCalfJSON) {
        new MakeHTTPRequest(
                "cows/add",
                newCalfJSON.toString(),
                response -> {
                    try {
                        Toast.makeText(this, "Added " + newCalfJSON.getString("tag_number"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(this, "Added calf", Toast.LENGTH_SHORT).show();
                    }
                    this.finish();
                },
                this
        ).execute();
    }

    private String makeIsoDate() {
        String year = String.valueOf(date.getYear());
        String month = String.valueOf(date.getMonth() + 1);
        String day = String.valueOf(date.getDayOfMonth());

        if (month.length() < 2)
            month = "0" + month;
        if (day.length() < 2)
            day = "0" + day;

        return year + "-" + month + "-" + day;
    }

    private void submit(View view) {
        Boolean bornEventEnabled = bornEvent.isChecked();
        Boolean calvedEventEnabled = calvedEvent.isChecked();
        String sire = sireSpinner.getSelectedItem().toString();
        String tagNumber = tagNumberEditText.getText().toString().trim();
        String owner = addCalfOwnerEditText.getText().toString().trim();
        String sex = sexSpinner.getSelectedItem().toString();
        String iso_date = makeIsoDate();

        Map newCalfDict = new HashMap();
        newCalfDict.put("dam", parentTagNumber);
        newCalfDict.put("sire", sire);
        newCalfDict.put("tag_number", tagNumber);
        newCalfDict.put("owner", owner);
        newCalfDict.put("sex", sex);
        newCalfDict.put("born_event", bornEventEnabled);
        newCalfDict.put("calved_event", calvedEventEnabled);
        newCalfDict.put("date", iso_date);

        JSONObject newCalfJSON = new JSONObject(newCalfDict);
        addCalf(newCalfJSON);
    }
}