package tech.jforseth.cattledatabase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.json.JSONObject;

/*
    I'm going to be honest, I'm not really sure what these PagerAdapters do.
    Basically, this just creates new InformationSublistFragments for each tab
    in the EventInformationActivity.
 */
public class CowTabPagerAdapter extends FragmentPagerAdapter {

    private final int tabCount;
    private JSONObject jsonInfo;
    private final Context context;

    public CowTabPagerAdapter(FragmentManager fm, int numberOfTabs, JSONObject jsonInfo, Context context) {
        super(fm);
        this.tabCount = numberOfTabs;
        this.context = context;
        this.jsonInfo = jsonInfo;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                InformationSublistFragment offspringFragment = new InformationSublistFragment().newInstance(jsonInfo, "calves", 0, 0, 1);
                offspringFragment.setListener(this::loadCalf);
                return offspringFragment;
            case 1:
                InformationSublistFragment eventFragment = new InformationSublistFragment().newInstance(jsonInfo, "events", 0, 1, 2);
                eventFragment.setListener(this::loadEvent);
                return eventFragment;
            case 2:
                InformationSublistFragment transactionFragment = new InformationSublistFragment().newInstance(jsonInfo, "transactions", 0, 1, 2);
                transactionFragment.setListener(this::loadTransaction);
                return transactionFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    private void loadTransaction(View view) {
        final String transaction_id = ((TextView) view).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "transactions/transaction",
                transaction_id,
                transaction_response -> {
                    Intent new_intent = new Intent(context, TransactionInformationActivity.class);
                    new_intent.putExtra("transaction_id", ((TextView) view).getText().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", transaction_response.toString());
                    context.startActivity(new_intent);
                    ((Activity) context).finish();

                },
                context
        ).execute();

    }

    private void loadEvent(View view) {
        final String event_id = ((TextView) view).getHint().toString();
        Toast.makeText(view.getContext(), "Loading: " + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "events/event",
                event_id,
                event_response -> {
                    Intent new_intent = new Intent(context, EventInformationActivity.class);
                    new_intent.putExtra("event_id", event_id);
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", event_response.toString());
                    context.startActivity(new_intent);
                    ((Activity) context).finish();

                },
                view.getContext()
        ).execute();
    }

    private void loadCalf(View view) {
        final String calf_id = ((TextView) view).getHint().toString();
        Toast.makeText(context, "Loading: " + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
        new MakeHTTPRequest(
                "cows/cow",
                calf_id,
                calf_response -> {
                    Intent new_intent = new Intent(context, CowInformationActivity.class);
                    new_intent.putExtra("event_id", ((TextView) view).getText().toString());
                    /*
                     * As far is I can tell, there is no way to send a JSONObject
                     * directly, so we send it as a string instead and parse it
                     * back on the other end.
                     */

                    new_intent.putExtra("json_data", calf_response.toString());
                    context.startActivity(new_intent);
                    ((Activity) context).finish();

                },
                context
        ).execute();

    }
}
