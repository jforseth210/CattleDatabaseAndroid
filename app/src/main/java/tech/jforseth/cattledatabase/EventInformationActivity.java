package tech.jforseth.cattledatabase;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Response;
import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
    Displays an individual event, and provides toggleable FABs
    for a variety of possible actions for that event.
    The tabbed lists are handled by EventTabPagerAdapter,
    and there are a number of other activities that deal with
    more complex operations.
 */
public class EventInformationActivity extends AppCompatActivity {
    private FloatingActionButton mEditFab, mAddRemoveCowsFab, mAddTransactionFab, mDeleteEventFab, mChangeNameFab, mChangeDescriptionFab, mChangeDateFab;
    private TextView mAddRemoveCowsActionText, mAddTransactionActionText, mDeleteEventActionText, mChangeNameActionText, mChangeDescriptionActionText, mChangeDateActionText;
    private Boolean isAllFabsVisible;

    private Intent intent;
    private JSONObject response;
    private String eventId;

    private TextView eventNameTextView, dateTextView, descriptionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_information);

        intent = getIntent();

        eventNameTextView = findViewById(R.id.eventInformationEventNameTextView);
        dateTextView = findViewById(R.id.eventInformationDateTextView);
        descriptionTextView = findViewById(R.id.eventInformationDescriptionTextView);

        try {
            response = new JSONObject(intent.getExtras().getString("json_data", ""));
            eventId = response.getString("event_id");

            eventNameTextView.setText(response.getString("name"));
            dateTextView.setText(response.getString("date"));
            descriptionTextView.setText(response.getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mEditFab = findViewById(R.id.transaction_edit_fab);
        mAddRemoveCowsFab = findViewById(R.id.add_remove_cows_fab);
        mAddTransactionFab = findViewById(R.id.change_amount_fab);
        mDeleteEventFab = findViewById(R.id.delete_transaction_fab);
        mChangeNameFab = findViewById(R.id.change_name_fab);
        mChangeDescriptionFab = findViewById(R.id.change_description_fab);
        mChangeDateFab = findViewById(R.id.change_to_from_fab);

        mAddRemoveCowsActionText = findViewById(R.id.add_remove_cows_action_text);
        mAddTransactionActionText = findViewById(R.id.change_amount_action_text);
        mDeleteEventActionText = findViewById(R.id.delete_transaction_action_text);
        mChangeNameActionText = findViewById(R.id.change_name_action_text);
        mChangeDescriptionActionText = findViewById(R.id.change_description_action_text);
        mChangeDateActionText = findViewById(R.id.change_to_from_action_text);

        hideFabs();

        isAllFabsVisible = false;
        mEditFab.setOnClickListener(view -> {
            toggleFabs();
        });
        mDeleteEventFab.setOnClickListener(this::showDeleteEventDialog);
        mAddRemoveCowsFab.setOnClickListener(this::addRemoveCows);
        mChangeNameFab.setOnClickListener(this::showChangeNameDialog);
        mChangeDescriptionFab.setOnClickListener(this::showChangeDescriptionDialog);
        mChangeDateFab.setOnClickListener(this::showChangeDateDialog);
        mAddTransactionFab.setOnClickListener(this::switchToAddTransactionActivity);

        // Information sublist sourcery
        final ViewPager viewPager =
                findViewById(R.id.eventInfoViewPager);

        TabLayout eventInformationTabs = findViewById(R.id.eventInformationTabs);
        final PagerAdapter adapter = new EventTabPagerAdapter
                (getSupportFragmentManager(),
                        eventInformationTabs.getTabCount(), response, this);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(eventInformationTabs));
        eventInformationTabs.setOnTabSelectedListener(new
                                                              TabLayout.OnTabSelectedListener() {
                                                                  @Override
                                                                  public void onTabSelected(TabLayout.Tab tab) {
                                                                      viewPager.setCurrentItem(tab.getPosition());
                                                                  }

                                                                  @Override
                                                                  public void onTabUnselected(TabLayout.Tab tab) {

                                                                  }

                                                                  @Override
                                                                  public void onTabReselected(TabLayout.Tab tab) {

                                                                  }

                                                              });


    }
    private void switchToAddTransactionActivity(View view){
        Intent i = new Intent(this, TransactionAddTransactionActivity.class);
        i.putExtra("event_id", eventId);
        this.startActivity(i);
    }
    private void populateDatePicker(DatePicker date) {
        try {
            String original_date = response.getString("date");
            int year = Integer.parseInt(original_date.substring(0, 4));
            int month = Integer.parseInt(original_date.substring(5, 7)) - 1;
            int day = Integer.parseInt(original_date.substring(8, 10));
            date.updateDate(year, month, day);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String makeIsoDate(DatePicker date) {
        String year = String.valueOf(date.getYear());
        String month = String.valueOf(date.getMonth() + 1);
        String day = String.valueOf(date.getDayOfMonth());

        if (month.length() < 2)
            month = "0" + month;
        if (day.length() < 2)
            day = "0" + day;

        return year + "-" + month + "-" + day;
    }

    private void showChangeDateDialog(View view) {
        DatePicker date = new DatePicker(this);
        populateDatePicker(date);
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Date")
                    .setView(date)
                    .setMessage("Select a new date for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String iso_date = makeIsoDate(date);
                        Map newDateDict = new HashMap();
                        newDateDict.put("event_id", eventId);
                        newDateDict.put("date", iso_date);
                        JSONObject newDateJSON = new JSONObject(newDateDict);
                        changeDate(newDateJSON, iso_date);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void changeDate(JSONObject newDateJSON, String iso_date) {
        new MakeHTTPRequest(
                "events/change_date",
                newDateJSON.toString(),
                change_date_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Date Changed")
                                .setMessage(response.getString("name") + " is now on " + iso_date)
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.eventInformationDateTextView)).setText(iso_date);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }

    private void showChangeDescriptionDialog(View view) {
        EditText input = new EditText(this);
        try {
            input.setText(response.getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Event Description")
                    .setView(input)
                    .setMessage("Enter a new description for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        String new_description = input.getText().toString().trim();
                        changeDescription(new_description);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void changeDescription(String new_description) {
        Map newDescriptionDict = new HashMap();
        newDescriptionDict.put("event_id", eventId);
        newDescriptionDict.put("new_name", new_description);
        JSONObject newNameJson = new JSONObject(newDescriptionDict);

        new MakeHTTPRequest(
                "events/change_description",
                newNameJson.toString(),
                chang_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Description Changed")
                                .setMessage(response.getString("name") + "'s description has been updated.")
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.eventInformationDescriptionTextView)).setText(new_description);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                this
        ).execute();
    }

    private void showChangeNameDialog(View view) {
        EditText input = new EditText(this);
        AlertDialog dialog = null;
        try {
            input.setText(response.getString("name"));

            dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Event Name")
                    .setView(input)
                    .setMessage("Enter a new name for " + response.getString("name") + ":")
                    .setNegativeButton("Cancel", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Change", (dialog1, which) -> {
                        changeName(input);
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void changeName(EditText input) {
        String new_name = input.getText().toString().trim();
        Map newNameDict = new HashMap();
        newNameDict.put("event_id", eventId);
        newNameDict.put("new_name", new_name);
        JSONObject newNameJson = new JSONObject(newNameDict);

        new MakeHTTPRequest(
                "events/change_name",
                newNameJson.toString(),
                change_tag_response -> {
                    AlertDialog success_dialog = null;
                    try {
                        success_dialog = new AlertDialog.Builder(this)
                                .setTitle("Name Changed")
                                .setMessage(response.getString("name") + " is now " + new_name)
                                .setPositiveButton("Ok", null)
                                .create();
                        ((TextView) findViewById(R.id.eventInformationEventNameTextView)).setText(new_name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    success_dialog.show();
                },
                error -> {
                    error.printStackTrace();
                },
                this
        ).execute();
    }

    private void showDeleteEventDialog(View view) {
        AlertDialog dialog = null;
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle("Delete Event")
                    .setMessage("Are you sure you want to delete " + response.getString("name") + "? This action is IRREVERSIBLE")
                    .setNegativeButton("No", (dialog1, which) -> {
                        //Do nothing
                    })
                    .setPositiveButton("Yes", (dialog1, which) -> {
                        try {
                            deleteEvent(eventId, response.getString("name"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    })
                    .create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void deleteEvent(String eventId, String name) {
        new MakeHTTPRequest(
                "events/delete",
                eventId,
                success_response -> {
                    AlertDialog success_dialog = null;
                    success_dialog = new AlertDialog.Builder(this)
                            .setTitle("Deleted")
                            .setMessage(name + " has been deleted")
                            .setPositiveButton("Ok", null)
                            .create();

                    success_dialog.show();
                    this.finish();
                },
                error_response -> {
                    try {
                        if (new JSONObject(new String(error_response.networkResponse.data)).getString("reason").equals("transactions_still_exist")) {
                            AlertDialog error_dialog = null;
                            error_dialog = new AlertDialog.Builder(this)
                                    .setTitle("Failed to delete event!")
                                    .setMessage("This event still has transactions attached! Please delete all of this events transactions before deleting the event.")
                                    .setPositiveButton("Ok", null)
                                    .create();
                            error_dialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                this
        ).execute();
    }

    private void addRemoveCows(View view) {
        // Load the list of all cows
        new MakeHTTPRequest(
                "cows/get_list",
                //These can't be lambda because generateCowList() won't accept the responses.
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject getCowsResponse) {
                        System.out.println("MAde it this far");
                        // Load list of cows from this event
                        new MakeHTTPRequest(
                                "events/event/get_cows",
                                eventId,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject eventGetCowsResponse) {
                                        List<KeyPairBoolData> cows = null;
                                        AlertDialog dialog = null;
                                        try {
                                            // Create List<KeyPairBoolData> of cows from JSON
                                            cows = generateCowList(getCowsResponse, eventGetCowsResponse);
                                            // Create a MultiSpinnerSearch object from the list of cows
                                            MultiSpinnerSearch multiSelectSpinnerWithSearch = makeMultiSelectSpinnerWithSearch(cows);
                                            showAddRemoveCowsDialog(multiSelectSpinnerWithSearch);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                EventInformationActivity.this
                        ).execute();
                    }
                },
                EventInformationActivity.this
        ).execute();
    }

    private MultiSpinnerSearch makeMultiSelectSpinnerWithSearch(List cows) {
        MultiSpinnerSearch multiSelectSpinnerWithSearch = new MultiSpinnerSearch(EventInformationActivity.this);
        multiSelectSpinnerWithSearch.setSearchEnabled(true);
        multiSelectSpinnerWithSearch.setSearchHint("Select Cows");
        multiSelectSpinnerWithSearch.setEmptyTitle("No matching cows");
        multiSelectSpinnerWithSearch.setShowSelectAllButton(true);
        multiSelectSpinnerWithSearch.setClearText("Close & Clear");
        multiSelectSpinnerWithSearch.setItems(cows, new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
            }
        });
        return multiSelectSpinnerWithSearch;
    }

    private void showAddRemoveCowsDialog(MultiSpinnerSearch multiSelectSpinnerWithSearch) throws JSONException {
        AlertDialog dialog = new AlertDialog.Builder(EventInformationActivity.this)
                .setTitle("Update Cows")
                .setView(multiSelectSpinnerWithSearch)
                .setMessage("Tap below to select the cows involved with " + response.getString("name"))
                .setNegativeButton("Cancel", (dialog1, which) -> {
                    //Do nothing
                })
                .setPositiveButton("Update", (dialog1, which) -> {
                    updateCows(multiSelectSpinnerWithSearch);
                })
                .create();
        dialog.show();
    }

    private void updateCows(MultiSpinnerSearch multiSelectSpinnerWithSearch) {
        ArrayList<String> selectedCows = new ArrayList<>();
        List<KeyPairBoolData> cows = multiSelectSpinnerWithSearch.getSelectedItems();
        for (int i = 0; i < cows.size(); i++) {
            selectedCows.add(cows.get(i).getName());
        }

        Map updateCowsDict = new HashMap();
        updateCowsDict.put("event_id", eventId);
        updateCowsDict.put("cows", selectedCows);
        JSONObject updateCowsJson = new JSONObject(updateCowsDict);
        new MakeHTTPRequest(
                "events/event/update_cows",
                updateCowsJson.toString(),
                EventInformationActivity.this
        ).execute();
    }

    private List<KeyPairBoolData> generateCowList(JSONObject getCowsResponse, JSONObject eventGetCowsResponse) throws JSONException {
        JSONArray getCowsJson;
        JSONArray eventGetCowsJson;
        List<KeyPairBoolData> cows = new ArrayList<>();
        getCowsJson = getCowsResponse.getJSONArray("cows");
        eventGetCowsJson = eventGetCowsResponse.getJSONArray("cows");
        if (getCowsJson != null) {
            // Iterate through all of the cows
            for (int i = 0; i < getCowsJson.length(); i++) {
                // Create a new KPBD object used by multi select spinner
                KeyPairBoolData cow = new KeyPairBoolData();
                // Cow tag number
                cow.setName(getCowsJson.getJSONArray(i).getString(0));
                // Array index
                cow.setId(i);

                Boolean selected = false;
                // Iterate through event's cows, if this cow is one of them, pre-select it
                for (int j = 0; j < eventGetCowsJson.length(); j++) {
                    if (getCowsJson.getJSONArray(i).getString(0).equals(eventGetCowsJson.getJSONArray(j).getString(0))) {
                        selected = true;
                        break;
                    }
                }
                cow.setSelected(selected);

                //Add the cow to the List
                cows.add(cow);
            }
        }
        return cows;
    }

    private void toggleFabs() {
        if (!isAllFabsVisible) {
            showFabs();
            isAllFabsVisible = true;
        } else {
            hideFabs();
            isAllFabsVisible = false;
        }
    }
    private void hideFabs(){
        mAddRemoveCowsFab.hide();
        mAddTransactionFab.hide();
        mDeleteEventFab.hide();
        mChangeNameFab.hide();
        mChangeDescriptionFab.hide();
        mChangeDateFab.hide();
        mAddRemoveCowsActionText.setVisibility(View.GONE);
        mAddTransactionActionText.setVisibility(View.GONE);
        mDeleteEventActionText.setVisibility(View.GONE);
        mChangeDescriptionActionText.setVisibility(View.GONE);
        mChangeNameActionText.setVisibility(View.GONE);
        mChangeDateActionText.setVisibility(View.GONE);
    }
    private void showFabs(){
        mAddRemoveCowsFab.show();
        mAddTransactionFab.show();
        mDeleteEventFab.show();
        mChangeNameFab.show();
        mChangeDescriptionFab.show();
        mChangeDateFab.show();
        mAddRemoveCowsActionText.setVisibility(View.VISIBLE);
        mAddTransactionActionText.setVisibility(View.VISIBLE);
        mDeleteEventActionText.setVisibility(View.VISIBLE);
        mChangeNameActionText.setVisibility(View.VISIBLE);
        mChangeDescriptionActionText.setVisibility(View.VISIBLE);
        mChangeDateActionText.setVisibility(View.VISIBLE);
    }
}