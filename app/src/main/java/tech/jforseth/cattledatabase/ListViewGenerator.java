package tech.jforseth.cattledatabase;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import org.json.JSONException;

import java.util.ArrayList;
/*
    Given arrays of identifiers, primary, secondary texts,
    as well as a listener. ListViewGenerator creates an
    ArrayList of CardViews with that information.

    This should be done using xml, not programmatically.
    However, I'm still learning and resorting to this awkward
    technique instead.
 */
public class ListViewGenerator {
    private final Context context;
    private final String[] idArray;
    private final String[] primaryTextArray;
    private final String[] secondaryTextArray;
    private final View.OnClickListener listener;

    /*
        idArray - An array of unique identifiers
        primaryTextArray - An array of values for the primary text field
        secondaryTextArray - An array of values for the secondary text field
        ALL THREE OF THESE SHOULD BE OF EQUIVALENT LENGTH

        listener - The OnClickListener for the primaryTextView
     */
    public ListViewGenerator(Context context, String[] idArray, String[] primaryTextArray, String[] secondaryTextArray, View.OnClickListener listener) {
        this.context = context;
        this.idArray = idArray;
        this.primaryTextArray = primaryTextArray;
        this.secondaryTextArray = secondaryTextArray;
        this.listener = listener;
    }

    public ArrayList<CardView> makeList() {
        ArrayList<CardView> cards = new ArrayList<CardView>();
        for (int i = 0; i < primaryTextArray.length; i++) {
            /*
             Programmatically designing a card and populating it
             with values from the three arrays and then adding it
             to the cards ArrayList.
             */
            CardView card = new CardView(context);
            TextView primaryText = new TextView(context);
            TextView secondaryText = new TextView(context);

            card.setCardBackgroundColor(context.getColor(R.color.blue));
            ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.height = 160;
            params.topMargin = 50;
            card.setLayoutParams(params);
            card.setRadius(25);

            primaryText.setTextColor(context.getColor(R.color.design_default_color_on_primary));
            primaryText.setText(primaryTextArray[i]);
            // A little hacky, but the identifier is stored as the hint in the primary TextView
            primaryText.setHint(idArray[i]);
            primaryText.setTextSize(20);
            primaryText.setPadding(30, 30, 0, 0);
            primaryText.setOnClickListener(this.listener);

            secondaryText.setText(this.secondaryTextArray[i]);
            secondaryText.setPadding(30, 95, 0, 0);
            secondaryText.setTextColor(context.getColor(R.color.design_default_color_on_primary));

            card.addView(primaryText);
            card.addView(secondaryText);
            cards.add(card);
        }
        return cards;
    }
}
