package tech.jforseth.cattledatabase;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
    This activity displays a form when the user wants to create a new
    cow. It prompts for all of the information necessary to create a
    cow and then sends that information to the server.
 */
public class CowAddCowActivity extends AppCompatActivity {
    private Spinner sexSpinner, sireSpinner, damSpinner;
    private EditText ownerEditText, tagNumberEditText;
    private Button submitButton;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow_add_cow);

        sexSpinner = findViewById(R.id.sexSpinner);
        sireSpinner = findViewById(R.id.sireSpinner);
        damSpinner = findViewById(R.id.damSpinner);
        ownerEditText = findViewById(R.id.editTextOwner);
        tagNumberEditText = findViewById(R.id.addCowEditTextTagNumber);
        submitButton = findViewById(R.id.addCowSubmitButton);

        preferences = getSharedPreferences("tech.jforseth.CattleDatabase", MODE_PRIVATE);

        populateInputs();

        submitButton.setOnClickListener(this::submit);
    }

    private void populateInputs() {
        ownerEditText.setText(preferences.getString("username", ""));
        getPossibleParents("dam");
        getPossibleParents("sire");

        new MakeHTTPRequest(
                "cows/sex_list",
                this::populateSexSpinner,
                this
        ).execute();
    }

    private void populateSexSpinner(JSONObject response) {
        try {
            JSONArray sexes_json = response.getJSONArray("sexes");
            String[] sexes = new String[sexes_json.length()];
            for (int i = 0; i < sexes_json.length(); i++) {
                sexes[i] = sexes_json.getString(i);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sexes);
            sexSpinner.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addCow(String newCowJSON) {
        new MakeHTTPRequest(
                "cows/add",
                newCowJSON,
                this
        ).execute();
    }

    private void getPossibleParents(String type) {
        new MakeHTTPRequest(
                "cows/possible_parents",
                type,
                this::parsePossibleParentsResponse,
                this
        ).execute();
    }

    private void parsePossibleParentsResponse(JSONObject response) {
        String parentType = null;
        try {
            parentType = response.getString("parent_type");
            JSONArray parentsJson = response.getJSONArray("parents");
            String[] parents = new String[parentsJson.length() + 1];
            parents[0] = "Not Applicable";
            for (int i = 0; i < parentsJson.length(); i++) {
                parents[i + 1] = parentsJson.getString(i);
            }
            populateParentSpinners(parentType, parents);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateParentSpinners(String parentType, String[] parents) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, parents);
        if (parentType.equals("dam")) {
            damSpinner.setAdapter(adapter);
        } else {
            sireSpinner.setAdapter(adapter);
        }
    }
    private void submit(View view){
        String dam = damSpinner.getSelectedItem().toString();
        String sire = sireSpinner.getSelectedItem().toString();
        String tagNumber = tagNumberEditText.getText().toString();
        String owner = ownerEditText.getText().toString();
        String sex = sexSpinner.getSelectedItem().toString();

        Map newCowDict = new HashMap();
        newCowDict.put("dam", dam);
        newCowDict.put("sire", sire);
        newCowDict.put("tag_number", tagNumber);
        newCowDict.put("owner", owner);
        newCowDict.put("sex", sex);

        JSONObject newCowJSON = new JSONObject(newCowDict);

        addCow(newCowJSON.toString());
        this.finish();
    }
}